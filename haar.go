package main

import (
	"flag"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"math"
	"os"
	"sort"
)

const V = 8

// Global variables for the flags because it's easier.
var fudge *uint
var scripted *bool
var debug *bool

// An array of RGB pixels.
type HaarColours struct {
	rh []float64
	gh []float64
	bh []float64
	gs []float64
}

type HaarChunk struct {
	index int
	major float64
	min0  float64
	min1  float64
	min2  float64
}

type RowDiff struct {
	left int
	right int
	diff float64
}
type RMSCrop struct {
	diff float64
	bounds image.Rectangle
}

type ByHaar []RowDiff
func (a ByHaar) Len() int { return len(a) }
func (a ByHaar) Swap(i, j int) { a[i],a[j] = a[j],a[i] }
func (a ByHaar) Less(i, j int) bool { return a[i].diff < a[j].diff }

type ByDiff []RMSCrop
func (a ByDiff) Len() int { return len(a) }
func (a ByDiff) Swap(i, j int) { a[i],a[j] = a[j],a[i] }
func (a ByDiff) Less(i, j int) bool { return a[i].diff < a[j].diff }

// Calculate the
// [Haar Transform](https://en.wikipedia.org/wiki/Haar_wavelet#Haar_transform_and_Inverse_Haar_transform)
// of a 1d list of floats. Algorithm taken from [here](https://unix4lyfe.org/haar/)
func haar(in []float64, depth int) []float64 {
	size := len(in)
	//scale := math.Sqrt(float64(size))

	// We need to work on a copy of our input For Reasons.
	out := make([]float64, size)
	copy(out, in[:])
	// `c <- c / sqrt(2^j)`
	//for i := 0; i < size; i++ {
	//	out[i] = out[i] / scale
	//}

	// Magic number for "do the whole list". Probably should panic
	// if it's not a power of 2.
	if depth == -1 {
		depth = len(in)
	}

	// We need to stuff things into the second half of the list.
	half := depth / 2

	for i := 0; i < half; i++ {
		left := in[2*i]
		right := in[2*i+1]
		sum := (left + right) / math.Sqrt2
		diff := (left - right) / math.Sqrt2
		out[i] = sum
		out[i+half] = diff
	}

	if half > 1 {
		out = haar(out, half)
	}

	return out
}

// Golang's `math` lib doesn't provide a `sgn` function.
// Although this could probably be better implemented by
// using `math.Signbit` rather than this code.
func sgn(x float64) int {
	if x == 0 {
		return 1
	}
	return int(float64(x) / math.Abs(float64(x)))
}

func makeHaar(xP []int, yP []int, size int, m image.Image, key string) HaarColours {
	// Output arrays.
	rp := make([]float64, size)
	gp := make([]float64, size)
	bp := make([]float64, size)
	gs := make([]float64, size)

	// Fetch the individual pixels from the image and convert them into
	// `float64`s.
	for i := 0; i < size; i++ {
		x := xP[i]
		y := yP[i]
		// We ignore alpha because 'I never use it' (c) Sean Lock
		r, g, b, _ := m.At(x, y).RGBA()

		// Reduce the bitsize if requested. This doesn't seem to make
		// much effect with the Haar but it did with the plain ol'
		// direct comparison code in `smartcropper`.
		if *fudge > 0 {
			r >>= *fudge
			g >>= *fudge
			b >>= *fudge
		}

		// Scale the colour values into a (0..1) range.
		// (nb. Presumably the 65535 should be scaled by `fudge` too?)
		rp[i], gp[i], bp[i] = float64(r)/65535, float64(g)/65535, float64(b)/65535
		gs[i] = 0.2126*rp[i] + 0.7152*gp[i] + 0.0722*bp[i]
	}

	// Calculate the Haar of each set of colour values.
	rh := haar(rp, -1)
	gh := haar(gp, -1)
	bh := haar(bp, -1)
	gg := haar(gs, -1)

	// And package it up all nice with a bow.
	return HaarColours{rh, gh, bh, gg}
}

// FUDGY FUDGE version of RMS.
func squareDiffs(l HaarColours, r HaarColours, howmany int) []float64 {
	sumR, sumG, sumB := 0.0, 0.0, 0.0

	// Only use the first 8 Haar coefficients for comparison.
	// (I wonder if there shouldn't be varying weights as we move
	// down the list? The first coefficient is by far the most
	// important. Hmm.)

	// Let's try a simple 'divide by 2' scaling.
	scale := 256.0

	// We'll divide the end result by how much scaling we've done.
	div := 0.0

	for i := 0; i < 8; i++ {
		dR := l.rh[i] - r.rh[i]
		dG := l.gh[i] - r.gh[i]
		dB := l.bh[i] - r.bh[i]
		sumR += dR * dR * scale
		sumG += dG * dG * scale
		sumB += dB * dB * scale
		div = div + scale
	//	scale = scale / math.Sqrt2
	}
	return []float64{sumR / div, sumG / div, sumB / div}
}

// Return the nearest power of 2 below an integer. Used to calculate
// the size of Haar transform we can do that fits into half the image.
func lowerPowerOf2(x int) int {
	f := math.Log2(float64(x))
	q := math.Pow(2, float64(int(f)))
	return int(q)
}

func genHaarRow(y, w, left int, m image.Image) HaarChunk {
	xP0 := make([]int, w)
	yP0 := make([]int, w)

	for i := 0; i < w; i++ {
		xP0[i] = left + i
		yP0[i] = y
	}

	r := makeHaar(xP0, yP0, w, m, "nuns")

	return HaarChunk{y, r.gs[0], r.gs[1], r.gs[2], r.gs[3]}
}

func genHaarCol(x, w, bottom int, m image.Image) HaarChunk {
	xP0 := make([]int, w)
	yP0 := make([]int, w)

	for i := 0; i < w; i++ {
		xP0[i] = x
		yP0[i] = i + bottom
	}

	r := makeHaar(xP0, yP0, w, m, "nuns")
	return HaarChunk{x, r.gs[0], r.gs[1], r.gs[2], r.gs[3]}
}

// Compare the Haar coefficients of two rows of pixels.
func compareHaarRow(x0, y0, y1, w int, m image.Image) float64 {
	xP0 := make([]int, w)
	yP0 := make([]int, w)
	yP1 := make([]int, w)

	for i := 0; i < w; i++ {
		xP0[i] = x0 + i
		yP0[i] = y0
		yP1[i] = y1
	}
	ck0 := fmt.Sprintf("row-%08X-%08X-%08X", y0, x0, w)
	ck1 := fmt.Sprintf("row-%08X-%08X-%08X", y1, x0, w)

	r0 := makeHaar(xP0, yP0, w, m, ck0)
	r1 := makeHaar(xP0, yP1, w, m, ck1)

	sqD := squareDiffs(r0, r1, w)
// fmt.Printf("%4d: %.4f %#v\n", y1, 0.2126*sqD[0] + 0.7152*sqD[1] + 0.0722*sqD[2], sqD)

	// I'm not convinced this is correct. Previously it did
	// `sqD[0] * sqD[1] * sqD[2]` but I'm not convinced that
	// was correct either.
	// rms := (sqD[0]+sqD[1]+sqD[2])/3
	rms := 0.2126*sqD[0] + 0.7152*sqD[1] + 0.0722*sqD[2]

	return rms
}

// Same as `compareHaarRow` but for columns.
func compareHaarCol(x0, x1, y0, w int, m image.Image) float64 {
	xP0 := make([]int, w)
	xP1 := make([]int, w)
	yP0 := make([]int, w)

	for i := 0; i < w; i++ {
		xP0[i] = x0
		xP1[i] = x1
		yP0[i] = y0 + i
	}
	ck0 := fmt.Sprintf("col-%08X-%08X-%08X", x0, y0, w)
	ck1 := fmt.Sprintf("col-%08X-%08X-%08X", x1, y0, w)

	r0 := makeHaar(xP0, yP0, w, m, ck0)
	r1 := makeHaar(xP1, yP0, w, m, ck1)

	sqD := squareDiffs(r0, r1, w)

	// Oh, consistency.
	rms := 0.2126*sqD[0] + 0.7152*sqD[1] + 0.0722*sqD[2]

	return rms
}

func main() {
	// Parse All The Options(tm)
	fudge = flag.Uint("fudge", 0, "Fudge factor")
	scripted = flag.Bool("machine", false, "Machine readable output")
	debug = flag.Bool("debug", false, "Emit reams of debugging output")
	flag.Parse()

	// Should probably loop over all the remaining arguments rather than
	// limiting ourselves to just a single file.
	filename := flag.Arg(0)
	reader, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()

	// LOAD ALL THE PIXELS.
	m, _, err := image.Decode(reader)
	if err != nil {
		log.Fatal(err)
	}

	// I've no idea why the `image` library doesn't return a strict
	// `(0,0) ... (x,y)` set of pixels but hey ho. Maybe there's some
	// random image format that doesn't have a `(0,0)` corner?
	b := m.Bounds()
	ll, rr, tt, bb := b.Min.X, b.Max.X, b.Max.Y, b.Min.Y
	height := tt-bb

	// Work out the middle of our image. Technically, this could be
	// an overflow if `ll + rr` is too big. But since we'd have either
	// a monstrous image or a really crazy bounding box, I'm not that
	// fussed about the possibility. (cf ["Nearly All Binary Searches
	// Are Broken"]
	//(https://research.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html))
	middleX := (ll + rr) / 2
	middleY := (tt + bb) / 2

	xP := make([]int, rr-ll)
	yP := make([]int, tt-bb)

	zz := make([]int, rr-ll+tt-bb)

	// Create a list of all the y-coords in the top half of our image.
	// This gets used in `makeHaar` later.
	i := 0
	for y := bb; y < middleY; y++ {
		yP[i] = y
		i++
	}

	rws := make([]HaarChunk, tt-bb)
	i = 0
	for y := bb; y < tt; y++ {
		rws[i] = genHaarRow(y, rr-ll, ll, m)
		i++
	}
// fmt.Printf("Comparing all left with all right\n")
	rows := make([]RowDiff, middleY*middleY)
	i = 0
	for l := bb; l < middleY; l++ {
		for r := middleY; r < tt; r++ {
			rows[i] = RowDiff{l-bb,r-bb,math.Inf(1)}
			i++
			if r-l < height/4 { continue}
			li,ri := l-bb, r-bb
			lv := rws[li]
			rv := rws[ri]
			diff := (math.Pow(lv.major-rv.major,2.0) * math.Pow(lv.min0-rv.min0,2.0) *math.Pow(lv.min1-rv.min1,2.0) *math.Pow(lv.min2-rv.min2,2.0))
			rows[i-1] = RowDiff{l-bb,r-bb,diff}
		}
	}
//fmt.Printf("%d comparisons for rows done\n", i)
	sort.Sort(ByHaar(rows))
	//fmt.Printf("%#v\n", rows[0:3])

	// Work out the right size later
	//cols := make([]RowDiff, 1048576)
	i = 0
	v := []RMSCrop{}
	// Iterate over the 10 best vertical matches
	for k := 0; k < 5; k++ {
		h := rows[k].right - rows[k].left
		w := rr-ll
		h2 := 2 * lowerPowerOf2(h)
		w2 := 2 * lowerPowerOf2(w)
		if h2 == 2*h { h2 = h }
		if w2 == 2*w { w2 = w }
w2 = w
		// Create a new image, padded to 2^x,2^y
		//fmt.Printf("%d (%d,%d) pad (%d,%d)\n", k, w,h, w2,h2)
		q := image.NewRGBA(image.Rect(0,0,w2-1,h2-1))
		for x := 0; x < w2; x++ {
			for y := 0; y < h2; y++ {
				// Wrap the image for padding
				px := x % w; py := y % h
				z := m.At(px+ll, py+bb)
				q.Set(x, y, z)
			}
		}
		if *debug {
			var opt jpeg.Options
			opt.Quality = 80
			fn := fmt.Sprintf("./test-%d.jpg", k)
			out, e := os.Create(fn)
			if e != nil { panic(e) }
			e = jpeg.Encode(out, q, &opt)
			if e != nil { panic(e) }
		}
		rws := make([]HaarChunk, w2)
		i = 0
		for x := 0; x < w2; x++ {
			rws[i] = genHaarCol(x, w2, 0, q)
			i++
		}
		cols := []RowDiff{} //nmake([]RowDiff, w2*w2/4)
		for l := 0; l < w2/2; l++ {
			for r := w2-1; r > l+w2/2; r-- {
				// If we're comparing a column with itself, don't
				if r - l < w2/2 { continue }
				lv := rws[l]
				rv := rws[r]
				diff := (math.Pow(lv.major-rv.major,2.0) * math.Pow(lv.min0-rv.min0,2.0)) + (math.Pow(lv.min1-rv.min1,2.0) + math.Pow(lv.min2-rv.min2,2.0))
				cols = append(cols, RowDiff{l,r,diff})
			}
		}
		sort.Sort(ByHaar(cols))
		for _, c := range cols[0:5] {
			w = c.right - c.left
			h = rows[k].right - rows[k].left
//			fmt.Printf("XX^%.18f^%d^%d^%.18f^-left %d -right %d -top %d -bottom %d\n", c.diff * rows[k].diff, w, h, float64(w)*float64(h)/(c.diff*rows[k].diff), c.left+ll, c.right+ll, rows[k].left+bb, rows[k].right+bb)
			v = append(v, RMSCrop{c.diff * rows[k].diff, image.Rect(c.left+ll,rows[k].left+bb,c.right+ll,rows[k].right+bb)})
		}
	}

	sort.Sort(ByDiff(v))
	//fmt.Printf("%#v\n", v[0])

	// Same for x-coords.
	i = 0
	for x := ll; x < middleX; x++ {
		xP[i] = x
		zz[i] = ll
		i++
	}

	// Find the maximum sized Haar we can perform on a quarter of this image.
	xW := lowerPowerOf2(rr-ll) / 2
	yW := lowerPowerOf2(tt-bb) / 2

	// This will break if I ever generate a JPEG that's a billion rows or columns.
	bestCol := 999999999
	bestRow := 999999999

	// Record the current best RMS we've seen. In retrospect, I'm thinking this is
	// not a good way to do this - might be better to generate all the RMS for the
	// 'matching' pairs and then sort that list. That then lets us use statistics
	// on the list (eg. 'pick the most stddev-y value') rather than a simple 'lowest
	// value'. I dunno, really.
	minRMS := math.Inf(1)

	for col := rr - 1; col > middleX; col-- {
		// Compare the left 2 columns against 2 from the right
		rms0 := compareHaarCol(ll, col, bb, yW, m)
		rms1 := compareHaarCol(ll+1, col+1, bb, yW, m)

		// I'm not sure this is the right calculation either.
		rms := rms0 * rms1

		// Is this a better match than we've seen before?
		if rms < minRMS {
			if *debug {
				fmt.Printf("COL + %d <=> %d %.4f,%.4f = %.4f < %.4f, moving from %d to %d\n", col, ll, rms0, rms1, rms, minRMS, bestCol, col)
				fmt.Printf("COL %d ADJACENT %.4f\n", col, rms1)
			}
			minRMS = rms
			bestCol = col - ll
		} else {
			if *debug {
				fmt.Printf("COL - %d <=> %d %.4f,%.4f = %.4f < %.4f, moving from %d to %d\n", col, ll, rms0, rms1, rms, minRMS, bestCol, col)
			}
		}
	}

	// Remember our best-matching column's RMS.
	colRMS := minRMS

	// RESET THE RMS.
	minRMS = math.Inf(1)

	for row := tt; row > middleY-50; row-- {
		rms0 := compareHaarRow(ll, bb, row, xW, m)
		rms := rms0
		if rms < minRMS {
			minRMS = rms
			bestRow = row - bb
		}
	}
	rowRMS := minRMS

	// I've no idea which of these is the best option.
	finalRMS := (colRMS) + (rowRMS)
	testRMS := (1 + colRMS) * (1 + rowRMS)

	// If we're outputting machine-readable information, make it nicely
	// caret-separated for easy `awk`ing.
	if *scripted {
		fmt.Printf("%s^%d^%d^%.4f^%.4f^%d^%d^%.8f^%.8f\n", filename, bestCol, bestRow, colRMS, rowRMS, rr, tt, testRMS, finalRMS)
	} else {
		fmt.Printf("-right %d -bottom %d # %.4f %.4f %d %d %.4f\n", bestCol, bestRow, colRMS, rowRMS, rr, tt, finalRMS)
	}
}
